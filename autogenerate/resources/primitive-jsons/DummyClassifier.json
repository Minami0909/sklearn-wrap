{
  "name": "sklearn.dummy.DummyClassifier",
  "id": "3bd7972674bcc642be7157107645ff2f",
  "common_name": "DummyClassifier",
  "is_class": true,
  "tags": [
    "dummy"
  ],
  "version": "0.19.1",
  "source_code": "https://github.com/scikit-learn/scikit-learn/blob/0.19.1/sklearn/dummy.py#L21",
  "parameters": [
    {
      "type": "str",
      "name": "strategy",
      "description": "Strategy to use to generate predictions.  * \"stratified\": generates predictions by respecting the training set\\'s class distribution. * \"most_frequent\": always predicts the most frequent label in the training set. * \"prior\": always predicts the class that maximizes the class prior (like \"most_frequent\") and ``predict_proba`` returns the class prior. * \"uniform\": generates predictions uniformly at random. * \"constant\": always predicts a constant label that is provided by the user. This is useful for metrics that evaluate a non-majority class  .. versionadded:: 0.17 Dummy Classifier now supports prior fitting strategy using parameter *prior*. "
    },
    {
      "type": "int",
      "optional": "true",
      "name": "random_state",
      "description": "If int, random_state is the seed used by the random number generator; If RandomState instance, random_state is the random number generator; If None, the random number generator is the RandomState instance used by `np.random`. "
    },
    {
      "type": "int",
      "shape": "n_outputs",
      "name": "constant",
      "description": "The explicit constant as predicted by the \"constant\" strategy. This parameter is useful only for the \"constant\" strategy. "
    }
  ],
  "attributes": [
    {
      "type": "array",
      "shape": "n_classes",
      "name": "classes_",
      "description": "Class labels for each output. "
    },
    {
      "type": "array",
      "shape": "n_classes",
      "name": "n_classes_",
      "description": "Number of label for each output. "
    },
    {
      "type": "array",
      "shape": "n_classes",
      "name": "class_prior_",
      "description": "Probability of each class for each output. "
    },
    {
      "type": "int",
      "name": "n_outputs_",
      "description": "Number of outputs. "
    },
    {
      "type": "bool",
      "name": "sparse_output_",
      "description": "True if the array returned from predict is to be in sparse CSC format. Is automatically set to True if the input y is passed in sparse format. "
    }
  ],
  "description": "'\nDummyClassifier is a classifier that makes predictions using simple rules.\n\nThis classifier is useful as a simple baseline to compare with other\n(real) classifiers. Do not use it for real problems.\n\nRead more in the :ref:`User Guide <dummy_estimators>`.\n",
  "methods_available": [
    {
      "name": "fit",
      "id": "sklearn.dummy.DummyClassifier.fit",
      "parameters": [
        {
          "type": "array-like, sparse matrix",
          "shape": "n_samples, n_features",
          "name": "X",
          "description": "Training vectors, where n_samples is the number of samples and n_features is the number of features. "
        },
        {
          "type": "array-like",
          "shape": "n_samples",
          "name": "y",
          "description": "Target values. "
        },
        {
          "type": "array-like",
          "shape": "n_samples",
          "optional": "true",
          "name": "sample_weight",
          "description": "Sample weights. "
        }
      ],
      "description": "'Fit the random classifier.\n",
      "returns": {
        "type": "object",
        "name": "self",
        "description": "Returns self. '"
      }
    },
    {
      "name": "get_params",
      "id": "sklearn.dummy.DummyClassifier.get_params",
      "parameters": [
        {
          "type": "boolean",
          "optional": "true",
          "name": "deep",
          "description": "If True, will return the parameters for this estimator and contained subobjects that are estimators. "
        }
      ],
      "description": "'Get parameters for this estimator.\n",
      "returns": {
        "type": "mapping",
        "name": "params",
        "description": "Parameter names mapped to their values. '"
      }
    },
    {
      "name": "predict",
      "id": "sklearn.dummy.DummyClassifier.predict",
      "parameters": [
        {
          "type": "array-like, sparse matrix",
          "shape": "n_samples, n_features",
          "name": "X",
          "description": "Input vectors, where n_samples is the number of samples and n_features is the number of features. "
        }
      ],
      "description": "'Perform classification on test vectors X.\n",
      "returns": {
        "type": "array",
        "shape": "n_samples",
        "name": "y",
        "description": "Predicted target values for X. '"
      }
    },
    {
      "name": "predict_log_proba",
      "id": "sklearn.dummy.DummyClassifier.predict_log_proba",
      "parameters": [
        {
          "type": "array-like, sparse matrix",
          "shape": "n_samples, n_features",
          "name": "X",
          "description": "Input vectors, where n_samples is the number of samples and n_features is the number of features. "
        }
      ],
      "description": "'\nReturn log probability estimates for the test vectors X.\n",
      "returns": {
        "type": "array-like",
        "shape": "n_samples, n_classes",
        "name": "P",
        "description": "Returns the log probability of the sample for each class in the model, where classes are ordered arithmetically for each output. '"
      }
    },
    {
      "name": "predict_proba",
      "id": "sklearn.dummy.DummyClassifier.predict_proba",
      "parameters": [
        {
          "type": "array-like, sparse matrix",
          "shape": "n_samples, n_features",
          "name": "X",
          "description": "Input vectors, where n_samples is the number of samples and n_features is the number of features. "
        }
      ],
      "description": "'\nReturn probability estimates for the test vectors X.\n",
      "returns": {
        "type": "array-like",
        "shape": "n_samples, n_classes",
        "name": "P",
        "description": "Returns the probability of the sample for each class in the model, where classes are ordered arithmetically, for each output. '"
      }
    },
    {
      "name": "score",
      "id": "sklearn.dummy.DummyClassifier.score",
      "parameters": [
        {
          "type": "array-like",
          "shape": "n_samples, n_features",
          "name": "X",
          "description": "Test samples. "
        },
        {
          "type": "array-like",
          "shape": "n_samples",
          "name": "y",
          "description": "True labels for X. "
        },
        {
          "type": "array-like",
          "shape": "n_samples",
          "optional": "true",
          "name": "sample_weight",
          "description": "Sample weights. "
        }
      ],
      "description": "'Returns the mean accuracy on the given test data and labels.\n\nIn multi-label classification, this is the subset accuracy\nwhich is a harsh metric since you require for each sample that\neach label set be correctly predicted.\n",
      "returns": {
        "type": "float",
        "name": "score",
        "description": "Mean accuracy of self.predict(X) wrt. y.  '"
      }
    },
    {
      "name": "set_params",
      "id": "sklearn.dummy.DummyClassifier.set_params",
      "parameters": [],
      "description": "\"Set the parameters of this estimator.\n\nThe method works on simple estimators as well as on nested objects\n(such as pipelines). The latter have parameters of the form\n``<component>__<parameter>`` so that it's possible to update each\ncomponent of a nested object.\n",
      "returns": {
        "name": "self",
        "description": "\""
      }
    }
  ]
}