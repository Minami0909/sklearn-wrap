{
  "name": "sklearn.semi_supervised.label_propagation.LabelSpreading",
  "id": "827abc511153fed8603c38650edeb275",
  "common_name": "LabelSpreading",
  "is_class": true,
  "tags": [
    "semi_supervised",
    "label_propagation"
  ],
  "version": "0.20.3",
  "source_code": "https://github.com/scikit-learn/scikit-learn/blob/0.20.3/sklearn/semi_supervised/label_propagation.py#L422",
  "parameters": [
    {
      "type": "'knn', 'rbf', callable",
      "name": "kernel",
      "description": "String identifier for kernel function to use or the kernel function itself. Only 'rbf' and 'knn' strings are valid inputs. The function passed should take two inputs, each of shape [n_samples, n_features], and return a [n_samples, n_samples] shaped weight matrix "
    },
    {
      "type": "float",
      "name": "gamma",
      "description": "parameter for rbf kernel "
    },
    {
      "type": "integer",
      "name": "n_neighbors",
      "description": "parameter for knn kernel "
    },
    {
      "type": "float",
      "name": "alpha",
      "description": "Clamping factor. A value in (0, 1) that specifies the relative amount that an instance should adopt the information from its neighbors as opposed to its initial label. alpha=0 means keeping the initial label information; alpha=1 means replacing all initial information. "
    },
    {
      "type": "integer",
      "name": "max_iter",
      "description": "maximum number of iterations allowed "
    },
    {
      "type": "float",
      "name": "tol",
      "description": "Convergence tolerance: threshold to consider the system at steady state "
    },
    {
      "type": "int",
      "optional": "true",
      "default": "None",
      "name": "n_jobs",
      "description": "The number of parallel jobs to run. ``None`` means 1 unless in a :obj:`joblib.parallel_backend` context. ``-1`` means using all processors. See :term:`Glossary <n_jobs>` for more details. "
    }
  ],
  "attributes": [
    {
      "type": "array",
      "shape": "n_samples, n_features",
      "name": "X_",
      "description": "Input array. "
    },
    {
      "type": "array",
      "shape": "n_classes",
      "name": "classes_",
      "description": "The distinct labels used in classifying instances. "
    },
    {
      "type": "array",
      "shape": "n_samples, n_classes",
      "name": "label_distributions_",
      "description": "Categorical distribution for each item. "
    },
    {
      "type": "array",
      "shape": "n_samples",
      "name": "transduction_",
      "description": "Label assigned to each item via the transduction. "
    },
    {
      "type": "int",
      "name": "n_iter_",
      "description": "Number of iterations run. "
    }
  ],
  "description": "\"LabelSpreading model for semi-supervised learning\n\nThis model is similar to the basic Label Propagation algorithm,\nbut uses affinity matrix based on the normalized graph Laplacian\nand soft clamping across the labels.\n\nRead more in the :ref:`User Guide <label_propagation>`.\n",
  "methods_available": [
    {
      "name": "fit",
      "id": "sklearn.semi_supervised.label_propagation.LabelSpreading.fit",
      "parameters": [
        {
          "type": "array-like",
          "shape": "n_samples, n_features",
          "name": "X",
          "description": "A {n_samples by n_samples} size matrix will be created from this "
        },
        {
          "type": "array",
          "shape": "n_samples",
          "name": "y",
          "description": "n_labeled_samples (unlabeled points are marked as -1) All unlabeled samples will be transductively assigned labels "
        }
      ],
      "description": "'Fit a semi-supervised label propagation model based\n\nAll the input data is provided matrix X (labeled and unlabeled)\nand corresponding label matrix y with a dedicated marker value for\nunlabeled samples.\n",
      "returns": {
        "type": "returns",
        "name": "self",
        "description": "'"
      }
    },
    {
      "name": "get_params",
      "id": "sklearn.semi_supervised.label_propagation.LabelSpreading.get_params",
      "parameters": [
        {
          "type": "boolean",
          "optional": "true",
          "name": "deep",
          "description": "If True, will return the parameters for this estimator and contained subobjects that are estimators. "
        }
      ],
      "description": "'Get parameters for this estimator.\n",
      "returns": {
        "type": "mapping",
        "name": "params",
        "description": "Parameter names mapped to their values. '"
      }
    },
    {
      "name": "predict",
      "id": "sklearn.semi_supervised.label_propagation.LabelSpreading.predict",
      "parameters": [
        {
          "type": "array",
          "shape": "n_samples, n_features",
          "name": "X",
          "description": ""
        }
      ],
      "description": "'Performs inductive inference across the model.\n",
      "returns": {
        "type": "array",
        "shape": "n_samples",
        "name": "y",
        "description": "Predictions for input data '"
      }
    },
    {
      "name": "predict_proba",
      "id": "sklearn.semi_supervised.label_propagation.LabelSpreading.predict_proba",
      "parameters": [
        {
          "type": "array",
          "shape": "n_samples, n_features",
          "name": "X",
          "description": ""
        }
      ],
      "description": "'Predict probability for each possible outcome.\n\nCompute the probability estimates for each single sample in X\nand each possible outcome seen during training (categorical\ndistribution).\n",
      "returns": {
        "type": "array",
        "shape": "n_samples, n_classes",
        "name": "probabilities",
        "description": "Normalized probability distributions across class labels '"
      }
    },
    {
      "name": "score",
      "id": "sklearn.semi_supervised.label_propagation.LabelSpreading.score",
      "parameters": [
        {
          "type": "array-like",
          "shape": "n_samples, n_features",
          "name": "X",
          "description": "Test samples. "
        },
        {
          "type": "array-like",
          "shape": "n_samples",
          "name": "y",
          "description": "True labels for X. "
        },
        {
          "type": "array-like",
          "shape": "n_samples",
          "optional": "true",
          "name": "sample_weight",
          "description": "Sample weights. "
        }
      ],
      "description": "'Returns the mean accuracy on the given test data and labels.\n\nIn multi-label classification, this is the subset accuracy\nwhich is a harsh metric since you require for each sample that\neach label set be correctly predicted.\n",
      "returns": {
        "type": "float",
        "name": "score",
        "description": "Mean accuracy of self.predict(X) wrt. y.  '"
      }
    },
    {
      "name": "set_params",
      "id": "sklearn.semi_supervised.label_propagation.LabelSpreading.set_params",
      "parameters": [],
      "description": "\"Set the parameters of this estimator.\n\nThe method works on simple estimators as well as on nested objects\n(such as pipelines). The latter have parameters of the form\n``<component>__<parameter>`` so that it's possible to update each\ncomponent of a nested object.\n",
      "returns": {
        "name": "self",
        "description": "\""
      }
    }
  ]
}