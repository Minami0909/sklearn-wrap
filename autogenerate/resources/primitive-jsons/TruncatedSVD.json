{
  "is_class": true, 
  "library": "sklearn", 
  "common_name": "Truncated SVD", 
  "compute_resources": {}, 
  "id": "87883321-14a0-3ea4-9723-09dd69d5c1a7", 
  "category": "decomposition.truncated_svd", 
  "source_code_unanalyzed": "https://github.com/scikit-learn/scikit-learn/blob/0.18.X/sklearn/decomposition/truncated_svd.py#L25", 
  "parameters": [
    {
      "type": "int", 
      "name": "n_components", 
      "description": "Desired dimensionality of output data. Must be strictly less than the number of features. The default value is useful for visualisation. For LSA, a value of 100 is recommended. "
    }, 
    {
      "type": "string", 
      "name": "algorithm", 
      "description": "SVD solver to use. Either \"arpack\" for the ARPACK wrapper in SciPy (scipy.sparse.linalg.svds), or \"randomized\" for the randomized algorithm due to Halko (2009). "
    }, 
    {
      "type": "int", 
      "optional": "true", 
      "name": "n_iter", 
      "description": "Number of iterations for randomized SVD solver. Not used by ARPACK. The default is larger than the default in `randomized_svd` to handle sparse matrices that may have large slowly decaying spectrum. "
    }, 
    {
      "type": "int", 
      "optional": "true", 
      "name": "random_state", 
      "description": "(Seed for) pseudo-random number generator. If not given, the numpy.random singleton is used. "
    }, 
    {
      "type": "float", 
      "optional": "true", 
      "name": "tol", 
      "description": "Tolerance for ARPACK. 0 means machine precision. Ignored by randomized SVD solver. "
    }
  ], 
  "tags": [
    "decomposition", 
    "truncated_svd"
  ], 
  "common_name_unanalyzed": "Truncated SVD", 
  "schema_version": 1.0, 
  "languages": [
    "python2.7"
  ], 
  "version": "0.18.1", 
  "build": [
    {
      "type": "pip", 
      "package": "scikit-learn"
    }
  ], 
  "description": "'Dimensionality reduction using truncated SVD (aka LSA).\n\nThis transformer performs linear dimensionality reduction by means of\ntruncated singular value decomposition (SVD). Contrary to PCA, this\nestimator does not center the data before computing the singular value\ndecomposition. This means it can work with scipy.sparse matrices\nefficiently.\n\nIn particular, truncated SVD works on term count/tf-idf matrices as\nreturned by the vectorizers in sklearn.feature_extraction.text. In that\ncontext, it is known as latent semantic analysis (LSA).\n\nThis estimator supports two algorithms: a fast randomized SVD solver, and\na \"naive\" algorithm that uses ARPACK as an eigensolver on (X * X.T) or\n(X.T * X), whichever is more efficient.\n\nRead more in the :ref:`User Guide <LSA>`.\n", 
  "methods_available": [
    {
      "id": "sklearn.decomposition.truncated_svd.TruncatedSVD.fit", 
      "returns": {
        "type": "object", 
        "name": "self", 
        "description": "Returns the transformer object. '"
      }, 
      "description": "'Fit LSI model on training data X.\n", 
      "parameters": [
        {
          "shape": "n_samples, n_features", 
          "type": "array-like, sparse matrix", 
          "name": "X", 
          "description": "Training data. "
        }
      ], 
      "name": "fit"
    }, 
    {
      "id": "sklearn.decomposition.truncated_svd.TruncatedSVD.fit_transform", 
      "returns": {
        "shape": "n_samples, n_components", 
        "type": "array", 
        "name": "X_new", 
        "description": "Reduced version of X. This will always be a dense array. '"
      }, 
      "description": "'Fit LSI model to X and perform dimensionality reduction on X.\n", 
      "parameters": [
        {
          "shape": "n_samples, n_features", 
          "type": "array-like, sparse matrix", 
          "name": "X", 
          "description": "Training data. "
        }
      ], 
      "name": "fit_transform"
    }, 
    {
      "id": "sklearn.decomposition.truncated_svd.TruncatedSVD.get_params", 
      "returns": {
        "type": "mapping", 
        "name": "params", 
        "description": "Parameter names mapped to their values. '"
      }, 
      "description": "'Get parameters for this estimator.\n", 
      "parameters": [
        {
          "type": "boolean", 
          "optional": "true", 
          "name": "deep", 
          "description": "If True, will return the parameters for this estimator and contained subobjects that are estimators. "
        }
      ], 
      "name": "get_params"
    }, 
    {
      "id": "sklearn.decomposition.truncated_svd.TruncatedSVD.inverse_transform", 
      "returns": {
        "shape": "n_samples, n_features", 
        "type": "array", 
        "name": "X_original", 
        "description": "Note that this is always a dense array. '"
      }, 
      "description": "'Transform X back to its original space.\n\nReturns an array X_original whose transform would be X.\n", 
      "parameters": [
        {
          "shape": "n_samples, n_components", 
          "type": "array-like", 
          "name": "X", 
          "description": "New data. "
        }
      ], 
      "name": "inverse_transform"
    }, 
    {
      "id": "sklearn.decomposition.truncated_svd.TruncatedSVD.set_params", 
      "returns": {
        "name": "self", 
        "description": "\""
      }, 
      "description": "\"Set the parameters of this estimator.\n\nThe method works on simple estimators as well as on nested objects\n(such as pipelines). The latter have parameters of the form\n``<component>__<parameter>`` so that it's possible to update each\ncomponent of a nested object.\n", 
      "parameters": [], 
      "name": "set_params"
    }, 
    {
      "id": "sklearn.decomposition.truncated_svd.TruncatedSVD.transform", 
      "returns": {
        "shape": "n_samples, n_components", 
        "type": "array", 
        "name": "X_new", 
        "description": "Reduced version of X. This will always be a dense array. '"
      }, 
      "description": "'Perform dimensionality reduction on X.\n", 
      "parameters": [
        {
          "shape": "n_samples, n_features", 
          "type": "array-like, sparse matrix", 
          "name": "X", 
          "description": "New data. "
        }
      ], 
      "name": "transform"
    }
  ], 
  "source_code": "https://github.com/scikit-learn/scikit-learn/blob/0.18.X/sklearn/decomposition/truncated_svd.py#L25", 
  "category_unanalyzed": "decomposition.truncated_svd", 
  "name": "sklearn.decomposition.truncated_svd.TruncatedSVD", 
  "team": "jpl", 
  "attributes": [
    {
      "shape": "n_components, n_features", 
      "type": "array", 
      "name": "components_", 
      "description": ""
    }, 
    {
      "type": "array", 
      "name": "explained_variance_ratio_", 
      "description": "Percentage of variance explained by each of the selected components. "
    }, 
    {
      "type": "array", 
      "name": "explained_variance_", 
      "description": "The variance of the training samples transformed by a projection to each component. "
    },
    {
      "type": "array",
      "name": "singular_values_",
      "description": "The singular values corresponding to each of the selected components. The singular values are equal to the 2-norms of the `n_components` variables in the lower-dimensional space."
    }
  ]
}
