{
  "name": "sklearn.kernel_ridge.KernelRidge",
  "id": "f31c4ce5ecee26468b6e64f3278f58ba",
  "common_name": "KernelRidge",
  "is_class": true,
  "tags": [
    "kernel_ridge"
  ],
  "version": "0.19.1",
  "source_code": "https://github.com/scikit-learn/scikit-learn/blob/0.19.1/sklearn/kernel_ridge.py#L16",
  "parameters": [
    {
      "type": "float, array-like",
      "shape": "n_targets",
      "name": "alpha",
      "description": "Small positive values of alpha improve the conditioning of the problem and reduce the variance of the estimates.  Alpha corresponds to ``(2*C)^-1`` in other linear models such as LogisticRegression or LinearSVC. If an array is passed, penalties are assumed to be specific to the targets. Hence they must correspond in number. "
    },
    {
      "type": "string",
      "name": "kernel",
      "description": "Kernel mapping used internally. A callable should accept two arguments and the keyword arguments passed to this object as kernel_params, and should return a floating point number. "
    },
    {
      "type": "float",
      "name": "gamma",
      "description": "Gamma parameter for the RBF, laplacian, polynomial, exponential chi2 and sigmoid kernels. Interpretation of the default value is left to the kernel; see the documentation for sklearn.metrics.pairwise. Ignored by other kernels. "
    },
    {
      "type": "float",
      "name": "degree",
      "description": "Degree of the polynomial kernel. Ignored by other kernels. "
    },
    {
      "type": "float",
      "name": "coef0",
      "description": "Zero coefficient for polynomial and sigmoid kernels. Ignored by other kernels. "
    },
    {
      "type": "mapping",
      "optional": "true",
      "name": "kernel_params",
      "description": "Additional parameters (keyword arguments) for kernel function passed as callable object. "
    }
  ],
  "attributes": [
    {
      "type": "array",
      "shape": "n_samples",
      "name": "dual_coef_",
      "description": "Representation of weight vector(s) in kernel space "
    },
    {
      "type": "array-like, sparse matrix",
      "shape": "n_samples, n_features",
      "name": "X_fit_",
      "description": "Training data, which is also required for prediction  References ---------- * Kevin P. Murphy \"Machine Learning: A Probabilistic Perspective\", The MIT Press chapter 14.4.3, pp. 492-493  See also -------- Ridge Linear ridge regression. SVR Support Vector Regression implemented using libsvm. "
    }
  ],
  "description": "'Kernel ridge regression.\n\nKernel ridge regression (KRR) combines ridge regression (linear least\nsquares with l2-norm regularization) with the kernel trick. It thus\nlearns a linear function in the space induced by the respective kernel and\nthe data. For non-linear kernels, this corresponds to a non-linear\nfunction in the original space.\n\nThe form of the model learned by KRR is identical to support vector\nregression (SVR). However, different loss functions are used: KRR uses\nsquared error loss while support vector regression uses epsilon-insensitive\nloss, both combined with l2 regularization. In contrast to SVR, fitting a\nKRR model can be done in closed-form and is typically faster for\nmedium-sized datasets. On the other  hand, the learned model is non-sparse\nand thus slower than SVR, which learns a sparse model for epsilon > 0, at\nprediction-time.\n\nThis estimator has built-in support for multi-variate regression\n(i.e., when y is a 2d-array of shape [n_samples, n_targets]).\n\nRead more in the :ref:`User Guide <kernel_ridge>`.\n",
  "methods_available": [
    {
      "name": "fit",
      "id": "sklearn.kernel_ridge.KernelRidge.fit",
      "parameters": [
        {
          "type": "array-like, sparse matrix",
          "shape": "n_samples, n_features",
          "name": "X",
          "description": "Training data "
        },
        {
          "type": "array-like",
          "shape": "n_samples",
          "name": "y",
          "description": "Target values "
        },
        {
          "type": "float",
          "shape": "n_samples",
          "name": "sample_weight",
          "description": "Individual weights for each sample, ignored if None is passed. "
        }
      ],
      "description": "'Fit Kernel Ridge regression model\n",
      "returns": {
        "type": "returns",
        "name": "self",
        "description": "'"
      }
    },
    {
      "name": "get_params",
      "id": "sklearn.kernel_ridge.KernelRidge.get_params",
      "parameters": [
        {
          "type": "boolean",
          "optional": "true",
          "name": "deep",
          "description": "If True, will return the parameters for this estimator and contained subobjects that are estimators. "
        }
      ],
      "description": "'Get parameters for this estimator.\n",
      "returns": {
        "type": "mapping",
        "name": "params",
        "description": "Parameter names mapped to their values. '"
      }
    },
    {
      "name": "predict",
      "id": "sklearn.kernel_ridge.KernelRidge.predict",
      "parameters": [
        {
          "type": "array-like, sparse matrix",
          "shape": "n_samples, n_features",
          "name": "X",
          "description": "Samples. "
        }
      ],
      "description": "'Predict using the kernel ridge model\n",
      "returns": {
        "type": "array",
        "shape": "n_samples",
        "name": "C",
        "description": "Returns predicted values. '"
      }
    },
    {
      "name": "score",
      "id": "sklearn.kernel_ridge.KernelRidge.score",
      "parameters": [
        {
          "type": "array-like",
          "shape": "n_samples, n_features",
          "name": "X",
          "description": "Test samples. "
        },
        {
          "type": "array-like",
          "shape": "n_samples",
          "name": "y",
          "description": "True values for X. "
        },
        {
          "type": "array-like",
          "shape": "n_samples",
          "optional": "true",
          "name": "sample_weight",
          "description": "Sample weights. "
        }
      ],
      "description": "'Returns the coefficient of determination R^2 of the prediction.\n\nThe coefficient R^2 is defined as (1 - u/v), where u is the residual\nsum of squares ((y_true - y_pred) ** 2).sum() and v is the total\nsum of squares ((y_true - y_true.mean()) ** 2).sum().\nThe best possible score is 1.0 and it can be negative (because the\nmodel can be arbitrarily worse). A constant model that always\npredicts the expected value of y, disregarding the input features,\nwould get a R^2 score of 0.0.\n",
      "returns": {
        "type": "float",
        "name": "score",
        "description": "R^2 of self.predict(X) wrt. y. '"
      }
    },
    {
      "name": "set_params",
      "id": "sklearn.kernel_ridge.KernelRidge.set_params",
      "parameters": [],
      "description": "\"Set the parameters of this estimator.\n\nThe method works on simple estimators as well as on nested objects\n(such as pipelines). The latter have parameters of the form\n``<component>__<parameter>`` so that it's possible to update each\ncomponent of a nested object.\n",
      "returns": {
        "name": "self",
        "description": "\""
      }
    }
  ]
}