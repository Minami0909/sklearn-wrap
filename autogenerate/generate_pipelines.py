from d3m.metadata.pipeline import Pipeline, PrimitiveStep
import d3m.primitives.datasets
import d3m.primitives.data
from d3m.metadata.base import ArgumentType, Context
from d3m.primitives.classification.bagging import SKlearn

# -> DatasetToDataFrame -> ColumnParser -> ExtractAttributes -> SKImputer -> SKRandomForestClassifier
#                                          ExtractTargets    ->              ^

# Creating pipeline
pipeline_description = Pipeline(context=Context.TESTING)
pipeline_description.add_input(name='inputs')

# # Step 1: DatasetToDataFrame
step_0 = PrimitiveStep(primitive_description=d3m.primitives.datasets.DatasetToDataFrame.metadata.query())
step_0.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='inputs.0')
step_0.add_output('produce')
pipeline_description.add_step(step_0)

# Step 2: ColumnParser
step_1 = PrimitiveStep(primitive_description=d3m.primitives.data.ColumnParser.metadata.query())
step_1.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.0.produce')
step_1.add_output('produce')
pipeline_description.add_step(step_1)
#
# # Step 3: ExtractAttributes
# step_2 = PrimitiveStep(primitive_description=d3m.primitives.data.ExtractColumnsBySemanticTypes.metadata.query())
# step_2.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.1.produce')
# step_2.add_output('produce')
# step_2.add_hyperparameter(name='semantic_types', argument_type=ArgumentType.VALUE,
#                           data=['https://metadata.datadrivendiscovery.org/types/Attribute'])
# pipeline_description.add_step(step_2)
#
# # Step 4: ExtractTargets
# step_3 = PrimitiveStep(primitive_description=d3m.primitives.data.ExtractColumnsBySemanticTypes.metadata.query())
# step_3.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.0.produce')
# step_3.add_output('produce')
# step_3.add_hyperparameter(name='semantic_types', argument_type=ArgumentType.VALUE,
#                           data=['https://metadata.datadrivendiscovery.org/types/TrueTarget'])
# pipeline_description.add_step(step_3)
#
# attributes = 'steps.2.produce'
# targets = 'steps.3.produce'

# Step 5: SKImputer
# step_4 = PrimitiveStep(primitive_description=d3m.primitives.sklearn_wrap.SKImputer.metadata.query())
# step_4.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference=attributes)
# step_4.add_output('produce')
# pipeline_description.add_step(step_4)

# Step 6: SKRandomForestClassifier
step_5 = PrimitiveStep(primitive_description=d3m.primitives.classification.bagging.SKlearn.metadata.query())
step_5.add_hyperparameter(name='use_semantic_types', argument_type=ArgumentType.VALUE,
                          data=True)
step_5.add_hyperparameter(name='add_index_columns', argument_type=ArgumentType.VALUE,
                          data=True)
step_5.add_argument(name='inputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.1.produce')
step_5.add_argument(name='outputs', argument_type=ArgumentType.CONTAINER, data_reference='steps.1.produce')
step_5.add_output('produce')
pipeline_description.add_step(step_5)

# Final Output
pipeline_description.add_output(name='output predictions', data_reference='steps.2.produce')
print(pipeline_description.to_json(indent=4, sort_keys=True, ensure_ascii=False))

with open("pipeline_description.json", "w") as fw:
    fw.write(pipeline_description.to_json(indent=4, sort_keys=True, ensure_ascii=False))
