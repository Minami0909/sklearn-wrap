from d3m.metadata import base as metadata_base, hyperparams as hyperparams_module, pipeline as pipeline_module, problem
from d3m.container.dataset import Dataset, D3MDatasetLoader
from d3m.runtime import Runtime
from d3m.metadata.base import Context
import os
import json
import sys
import json
import glob
from subprocess import check_output
import argparse
import subprocess
import gzip
import shutil
from subprocess import check_call

#python3 -m d3m runtime -d /home/files/new_datasets/datasets/seed_datasets_current fit-score -m .meta -p .json
#python run_pipeline.py --pipeline_dir /Users/alicery/Documents/d3m/sklearn-wrap/autogenerate/2020.1.9/JPL --dataset_path /Users/alicery/Documents/d3m/new_datasets/datasets/seed_datasets_current
#git lfs track "*.gz"
def main(args):

    pipeline_dir = str(args.pipeline_dir)

    dataset_path = args.dataset_path
    run_fitscore_pipeline(dataset_dir=dataset_path, pipeline_dir=pipeline_dir)

    # # Get meta file for datasets
    # dataset = ""
    # meta_filepath = ""
    # for file in glob.glob(os.path.join(pipeline_dir, "*.meta")):
    #     meta_filepath = file
    #
    # pipeline_file = meta_filepath.replace(".meta", ".json")
    #
    # if os.path.exists(meta_filepath):
    #     meta_file = json.load(open(meta_filepath, 'r'))
    #     dataset = str(meta_file.get("full_inputs", [])[0]).replace("_dataset", "")
    #
    # if dataset == "":
    #     print("No dataset could be selected from metafile {}".format(meta_filepath))
    #     exit(1)
    # datasets_base_dir = args[1]
    # # Loading problem
    # problem_description = problem.parse_problem_description(os.path.join(datasets_base_dir,
    #                                                                      'seed_datasets_current/{}/TRAIN/problem_TRAIN/problemDoc.json'.format(dataset)))
    # path = 'file://{uri}'.format(uri=os.path.abspath(os.path.join(datasets_base_dir,
    #                                                               'seed_datasets_current/{}/TRAIN/dataset_TRAIN/datasetDoc.json'.format(dataset))))
    # # if 'regression' in data["steps"][2]["primitive"]["python_path"]:
    # #     problem_description = problem.parse_problem_description(os.path.join(datasets_base_dir, 'datasets/seed_datasets_current/26_radon_seed/TRAIN/problem_TRAIN/problemDoc.json'))
    # #     path = 'file://{uri}'.format(uri=os.path.abspath(os.path.join(datasets_base_dir, 'datasets/seed_datasets_current/26_radon_seed/TRAIN/dataset_TRAIN/datasetDoc.json')))
    # #
    # # elif 'classification' in data["steps"][2]["primitive"]["python_path"]:
    # #     problem_description = problem.parse_problem_description(os.path.join(datasets_base_dir, 'datasets/seed_datasets_current/38_sick/TRAIN/problem_TRAIN/problemDoc.json'))
    # #     path = 'file://{uri}'.format(uri=os.path.abspath(os.path.join(datasets_base_dir, 'datasets/seed_datasets_current/38_sick/TRAIN/dataset_TRAIN/datasetDoc.json')))
    # #
    # # else:
    # #     problem_description = problem.parse_problem_description(os.path.join(datasets_base_dir, 'datasets/seed_datasets_current/27_wordLevels/TRAIN/problem_TRAIN/problemDoc.json'))
    # #     path = 'file://{uri}'.format(uri=os.path.abspath(os.path.join(datasets_base_dir, 'datasets/seed_datasets_current/27_wordLevels/TRAIN/dataset_TRAIN/datasetDoc.json')))
    #
    # dataset = D3MDatasetLoader()
    # dataset = dataset.load(dataset_uri=path)
    # print(dataset.metadata.query(()))
    #
    # # Loading pipeline description file.
    #
    # with open(pipeline_file, 'r') as file:
    #     pipeline_description = pipeline_module.Pipeline.from_json(string_or_file=file)
    #
    # # Creating an instance on runtime with pipeline description and problem description.
    # runtime = Runtime(pipeline=pipeline_description, problem_description=problem_description, context=Context.TESTING)
    #
    # # Fitting pipeline on input dataset.
    # #fit_outputs = runtime.fit(inputs=[dataset], return_values=['steps.0.produce', 'steps.1.produce'])
    # fit_outputs = runtime.fit(inputs=[dataset])
    #
    # # Producing results using the fitted pipeline.
    # produce_outputs = runtime.produce(inputs=[dataset])
    # print(produce_outputs.values)
    # print(args)
    # #print(json.dumps(fit_outputs.pipeline_run._to_json_structure(), indent=2))


def run_fitscore_pipeline(dataset_dir, pipeline_dir):
    """
    Needs datasets locally downloaded and scoring file downloded.
    This function is inspired by https://gitlab.com/datadrivendiscovery/common-primitives/blob/8b0c07ef137e6b09ba35e89e0bbd3d1f17f71d27/run_pipelines.sh
    :param dataset_dir:
    :param pipeline_dir:
    :return:
    """
    meta_files = list()

    for file in glob.glob(os.path.join(pipeline_dir, "**/**/**/*.meta")):
        meta_files.append(file)

    for file in meta_files:
        pipeline_file = str(file).replace(".meta", ".json")
        meta_file = json.load(open(file, 'r'))
        dataset = str(meta_file.get("full_inputs", [])[0]).replace("_dataset", "")
        problem_path = 'file://{uri}'.format(uri=os.path.abspath(os.path.join(dataset_dir,
                                                                      '{}/{}_problem/problemDoc.json'.format(
                                                                          dataset,dataset))))
        train_path = 'file://{uri}'.format(uri=os.path.abspath(os.path.join(dataset_dir,
                                                                      '{}/TRAIN/dataset_TRAIN/datasetDoc.json'.format(
                                                                          dataset))))
        test_path = 'file://{uri}'.format(uri=os.path.abspath(os.path.join(dataset_dir,
                                                                      '{}/TEST/dataset_TEST/datasetDoc.json'.format(
                                                                          dataset))))
        scoring_path = 'file://{uri}'.format(uri=os.path.abspath(os.path.join(dataset_dir,
                                                                      '{}/SCORE/dataset_TEST/datasetDoc.json'.format(
                                                                          dataset))))
        pipeline_run_file = str(os.path.dirname(file)).replace("pipelines", "pipeline_runs")
        os.makedirs(pipeline_run_file, exist_ok=True)
        pipeline_run_path = os.path.abspath(os.path.join(pipeline_run_file, "pipeline_run.yml"))

        cmdline_args = ["python3",
                        "-m",
                        "d3m",
                        "runtime",
                        "fit-score",
                        "--pipeline",
                        pipeline_file,
                        "-r",
                        problem_path,
                        "--input",
                        train_path,
                        "--test-input",
                        test_path,
                        "--score-input",
                        scoring_path,
                        "--output-run",
                        pipeline_run_path
                        ]
        print(cmdline_args)
        p = subprocess.run(cmdline_args)
        print(p.stdout)
        # with open(pipeline_run_path, 'rb') as f_in:
        #     with gzip.open('{}.gz'.format(pipeline_run_path), 'wb') as f_out:
        #         shutil.copyfileobj(f_in, f_out)
        check_call(['gzip', pipeline_run_path])

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--pipeline_dir", help="Location of scoring file", required=True)
    parser.add_argument("--dataset_path", help="the batch of datasets to run on", required=True)
    args = parser.parse_args()
    main(args)