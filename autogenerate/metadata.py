
# TODO: Define more mappings.
HYPERPARAM_SEMANTICTYPE_MAPPING = {
    "n_jobs": ["https://metadata.datadrivendiscovery.org/types/ResourcesUseParameter", "https://metadata.datadrivendiscovery.org/types/CPUResourcesUseParameter"],
    "cache_size": "https://metadata.datadrivendiscovery.org/types/ResourcesUseParameter",
    "leaf_size": ["https://metadata.datadrivendiscovery.org/types/ResourcesUseParameter","https://metadata.datadrivendiscovery.org/types/TuningParameter"],
    "precompute": "https://metadata.datadrivendiscovery.org/types/ResourcesUseParameter",
    "axis": "https://metadata.datadrivendiscovery.org/types/ControlParameter",
    "add_indicator":"https://metadata.datadrivendiscovery.org/types/ControlParameter",
    "missing_values": "https://metadata.datadrivendiscovery.org/types/ControlParameter",
    "w_init": "https://metadata.datadrivendiscovery.org/types/ControlParameter",
    "presort": "https://metadata.datadrivendiscovery.org/types/ControlParameter",
}

TUNING_HYPERPARAM = "https://metadata.datadrivendiscovery.org/types/TuningParameter"
