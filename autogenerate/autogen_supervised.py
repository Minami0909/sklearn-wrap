from jinja2 import Template, Environment, PackageLoader
from autogenerate import AutogenUtil
import os, os.path, json, collections, inspect, re, sklearn
from d3m.metadata import base as metadata_module
import pkgutil
from autogenerate import metadata
import uuid
# Says unused but need it for inspection
from sklearn import *

CONSTRUCTOR_ARGS = 'Constructor_Args'


def is_property(module, klass, attr):
    try:
        module = module.split(".")
        module.remove("sklearn")
        base_module = sklearn
        for m in module:
            class_obj = getattr(base_module, m)
            base_module = class_obj
        class_obj = getattr(base_module, klass)
        att_obj = getattr(class_obj, attr)

        return isinstance(att_obj, property)
    except:
        return False


def get_mapped_type(type):
    ndarray_types = ['list', 'array']
    int_type = ['int', 'integer']

    if type == 'array-like':
        return 'ndarray'
    elif type in ndarray_types:
        return 'ndarray'
    elif type in int_type:
        return 'int'
    elif type in ['ndarray', 'float', 'object', 'complex', 'str', 'tuple', 'range', 'list', 'bool']:
        return type
    elif type == 'boolean':
        return 'bool'

    return type


def get_template_vars_from_primitive_file(json_file, overlay, tag, primitive_family: str, version, primitive_ids, hyperparams_to_tune):
    if type(json_file) == str:
        json_file = open(json_file, 'r')
    return get_template_vars_from_primitive_json(json.load(json_file), overlay, tag, primitive_family, version, primitive_ids, hyperparams_to_tune)


def map_semantic_type(hyperparam, parent=None):
    type = hyperparam.get('type')
    name = hyperparam.get('name')
    semantic_type = []
    type_map = {
        "Uniform": "http://schema.org/Float",
        "LogUniform": "http://schema.org/Float",
        "UniformInt": "http://schema.org/Integer"
    }
    structural_map = {
        "str": "http://schema.org/Text",
        "int": "http://schema.org/Integer",
        "bool": "http://schema.org/Boolean"
    }
    if type in type_map:
        # semantic_type.append(type_map.get(type))
        pass
    elif hyperparam['init_args'].get('_structural_type') in structural_map:
        # semantic_type.append(structural_map.get(hyperparam['init_args']['_structural_type']))
        if hyperparam['init_args']['_structural_type'] == 'bool':
            hyperparam['type'] = 'UniformBool'
    # else:
    #     print("Not found {}".format(hyperparam))
    #     semantic_type.append("http://schema.org/Text")
    if name in metadata.HYPERPARAM_SEMANTICTYPE_MAPPING:
        if isinstance(metadata.HYPERPARAM_SEMANTICTYPE_MAPPING.get(name), list):
            semantic_type.extend(metadata.HYPERPARAM_SEMANTICTYPE_MAPPING.get(name))
        else:
            if parent in metadata.HYPERPARAM_SEMANTICTYPE_MAPPING:
                semantic_type.append(metadata.HYPERPARAM_SEMANTICTYPE_MAPPING.get(parent))
            elif name in metadata.HYPERPARAM_SEMANTICTYPE_MAPPING:
                semantic_type.append(metadata.HYPERPARAM_SEMANTICTYPE_MAPPING.get(name))
    else:
        semantic_type.append(metadata.TUNING_HYPERPARAM)
    hyperparam['init_args']['semantic_types'] = semantic_type
    return hyperparam


# To generate the params for the input template from the json files
def process_template_params(primitive: dict, overlay: dict, class_name: str, module_name: str):
    template_vars = {}
    params = {}
    overlay_param_dict = {}
    for param in primitive.get('attributes', []):
        overlay_param_dict.update({param.get('name'): param})

    for param in overlay.get('Params', []):
        overlay_param_dict.update({param.get('name'): param})

    for key in overlay_param_dict:
        attribute = overlay_param_dict.get(key)
        name = attribute.get('name')
        _type = attribute.get('type')
        if not is_property(module_name, class_name, name):
            params[name] = get_mapped_type(_type)
            if re.match(".*ndarray\[.*\]", _type):
                _type = 'ndarray'
                params[name] = _type
    template_vars.update({'params': params})
    return template_vars


# To remove verbosity arg
def remove_arg_from_constructor(constructor_args: list, arg_to_be_removed='verbosity'):
    for arg in constructor_args:
        if arg.get('name') == arg_to_be_removed:
            constructor_args.remove(arg)
    return constructor_args


# Mapping semantic types, handling strings and non-strings, formatting description.
def process_hyperparameter(h, parent=None):

    # Not having semantic types for now
    if h.get('init_args').get('semantic_types'):
        del h['init_args']['semantic_types']

    map_semantic_type(h, parent)

    # To correctly generate string and non string defaults in the wrapper code
    escaped_defaults = {'True', 'False', 'sklearn', 'None'}
    ESCAPE_CHAR = "&esc"
    if h.get('init_args').get('default') is not None:
        if isinstance(h.get('init_args').get('default'), str):
            is_str = True
            if ESCAPE_CHAR in h.get('init_args').get('default'):
                h['init_args']['default'] = "{}".format(h.get('init_args').get('default').replace(ESCAPE_CHAR, ""))
                is_str = False
            for escape in escaped_defaults:
                if escape in h.get('init_args').get('default'):
                    is_str = False
            if is_str:
                h['init_args']['default'] = "'{}'".format(h.get('init_args').get('default').replace("'", ""))

    # Preparing description.
    if 'description' in h.get('init_args', {}):
        desc = h['init_args']['description'].strip()
        if desc.startswith('"') and desc.endswith('"') or desc.startswith("'") and desc.endswith("'"):
            desc = desc[1:-1]
        if len(desc) == 0:
            del h['init_args']['description']
        else:
            h['init_args']['description'] = "'{}'".format(str(desc).replace("\\", "").replace("'", "\\'"))


# To generate the hyperparams from the input json files
def process_template_hyperparams(primitive: dict, overlay: dict):
    template_vars = {}
    hyperparams = list()
    for h in overlay.get('Hyperparams', []):
        # If verbose or random_state put it in constructor_args
        # if h['name'] == 'verbose' or h.get('name') == 'random_state':
        #     c_args = overlay.get(CONSTRUCTOR_ARGS, [])
        #     c_args.append(h)
        #     overlay[CONSTRUCTOR_ARGS] = c_args
        #     continue
        if h.get('init_args') is None:
            continue
        param = list(filter(lambda p: p['name'] == h['name'], primitive['parameters']))
        if len(param) > 0:
            h['init_args']['description'] = str(param[0]['description'])

        process_hyperparameter(h)

        if h.get('type') == 'Union':
            for h2 in h.get('hyperparams', []):
                process_hyperparameter(h2, h.get('name'))

        if h.get('type') == 'Choice':
            for h2 in h.get('hyperparams', {}).values():
                process_hyperparameter(h2)

                if h2.get('type') == 'Union':
                    for h3 in h2.get('hyperparams', []):
                        process_hyperparameter(h3, h2.get('name'))

        # print(h)
        hyperparams.append(h)
    # Remove standard arguments from the hyperparams and put them in constructor args
    STANDARD_CONSTRUCTOR_ARGS = ["verbose", "random_state"]
    constructor_args = []
    constructor_arg_names = set()
    type_map = {
        "Uniform": "float",
        "LogUniform": "float",
        "UniformInt": "int"
    }
    for h in list(hyperparams):
        if h.get('isConstructorArg'):
            args = {'name': h.get('name')}
            args['type'] = get_mapped_type(h.get('init_args').get('_structural_type'))
            args['default'] = h.get('init_args').get('default')
            if args.get('name') not in constructor_arg_names:
                constructor_args.append(args)
                constructor_arg_names.add(args.get('name'))
            hyperparams.remove(h)

        elif h.get('name') in metadata_module.STANDARD_RUNTIME_ARGUMENTS or h.get('name') in STANDARD_CONSTRUCTOR_ARGS:
            args = {'name': h.get('name')}  # args is a dict with keys name, type, default
            # args.update(metadata_base.STANDARD_RUNTIME_ARGUMENTS.get(h.get('name')))
            # args['type'] = re.match("<class '(.*)'>", str(args.get('type'))).group(1)
            if h.get('type') in type_map:
                args['type'] = type_map.get(h.get('type'))
            else:
                args['type'] = get_mapped_type(h.get('init_args').get('_structural_type'))
            args['default'] = h.get('init_args').get('default')

            if args.get('name') not in constructor_arg_names:
                constructor_args.append(args)
                constructor_arg_names.add(args.get('name'))
            hyperparams.remove(h)
    # Get any constructor args from the overlay
    for c_arg in overlay.get(CONSTRUCTOR_ARGS, []):
        if c_arg.get('init_args') and c_arg.get('init_args').get('default') is not None:
            c_arg['default'] = c_arg.get('init_args').get('default')
        exists = False
        if c_arg.get('name') not in constructor_arg_names:
                constructor_args.append(c_arg)
                constructor_arg_names.add(c_arg.get('name'))

    # check for standard constructor args in the parsed json if missed in overlay
    # for eg. random_state
    for param in primitive.get('parameters'):
        if param.get('name') in STANDARD_CONSTRUCTOR_ARGS:
            arg = {'name': param.get('name')}
            arg['type'] = get_mapped_type(param.get('type'))
            arg['default'] = param.get('default')
            if param.get('default') is None and param.get('type') == 'int':
                arg['default'] = 0
            if arg.get('name') not in constructor_arg_names:
                constructor_args.append(arg)
                constructor_arg_names.add(arg.get('name'))


    template_vars.update({'hyperparams': hyperparams})
    template_vars.update({'constructor_args': constructor_args})
    return template_vars


def process_predict_proba(primitive: dict):
    for method in primitive.get("methods_available", []):
        if 'predict_log_proba' in method.get("name"):
            return {"contains_log_proba": True}
        elif "predict_proba" in method.get("name"):
            return {"contains_proba": True}
    return {}


def process_partial_fit(primitive: dict):
    for method in primitive.get("methods_available", []):
        if 'partial_fit' in method.get("name"):
            return {"partial_fit": True}
    return {}


def replace_strings_to_enum(primitive: dict):
    algorithm_types = []
    for alg_type in primitive.get("algorithm_types", []):
        alg_type = "metadata_base.PrimitiveAlgorithmType.{}".format(alg_type)
        algorithm_types.append(alg_type)
    primitive.update({"algorithm_types": algorithm_types})

def contains_get_support(primitive: dict):
    for method in primitive.get("methods_available", []):
        if 'get_support' in method.get("name"):
            return {"contains_get_support": True}
    return {}

def contains_cluster_centers(primitive: dict):
    for attr in primitive.get("attributes", []):
        if attr.get("name", "") == 'cluster_centers_':
            return {"contains_cluster_centers": True}
    return {}


def contains_feature_importances(primitive: dict):
    for method in primitive.get("methods_available", []):
        if 'feature_importances_' in method.get("name"):
            return {"contains_feature_importances": True}
    return {}


def get_template_vars_from_primitive_json(primitive: dict, overlay: dict, tag: str, primitive_family: str, version,
                                          primitive_ids: dict=None, hyperparams_to_tune: dict=None):
    template_vars = {}

    name = primitive.get('name')
    class_name = class_name_override = name.split('.')[-1]
    module_name = name.replace("." + class_name, "")

    template_vars['from_modules'] = {module_name:class_name}

    if class_name not in overlay:
        # print("{} not found in overlay file. Need to provide Hyperparameter info".format(class_name))
        return None, None

    # Check if we have to skip generation
    if overlay.get(class_name).get("skip", False):
        print("Skipping {}".format(class_name))
        return None, None

    if overlay.get(class_name).get("class_name_override"):
        class_name_override = overlay.get(class_name).get("class_name_override")

    # Set template to use
    template_vars['sklearn_class_name'] = class_name
    template_vars['class_name'] = class_name_override
    template_vars['template'] = overlay.get(class_name).get('template', overlay.get('template'))
    template_vars['test_template'] = overlay.get(class_name).get('test_template', overlay.get('test_template'))
    template_vars['pipeline_template'] = overlay.get(class_name).get('pipeline_template', overlay.get('pipeline_template'))
    template_vars['primitive_family'] = overlay.get(class_name).get('primitive_family',overlay.get('primitive_family'))

    template_vars.update(process_template_params(primitive, overlay.get(class_name), class_name, module_name))

    if 'parameters' in primitive:
        template_vars.update(process_template_hyperparams(primitive, overlay.get(class_name)))

    if 'flags' in overlay.get(class_name):
        template_vars['flags'] = overlay.get(class_name).get('flags')

    if 'custom_imports' in overlay.get(class_name):
        template_vars.update({'custom_imports': overlay.get(class_name).get('custom_imports')})

    if 'test_data' in overlay.get(class_name):
        template_vars.update({'test_data': overlay.get(class_name).get('test_data')})

    metadata_keys = ["algorithm_types", "name", "primitive_family", "python_path", "source", "version", 'id', "hyperparams_to_tune"]


    metadata = {}
    split_class_name = re.sub(r'([A-Z])([a-z])', r'_\1\2', class_name_override)
    split_class_name = re.sub(r'([a-z])([A-Z])', r'\1_\2', split_class_name)
    split_class_name = re.sub('^[^A-Za-z]*', '', split_class_name)

    # Do not repeat primitive family in primitive name
    primitive_families = ["classifier", "regressor", "regression"]
    split_class_name = split_class_name.lower()
    for family in primitive_families:
        if family in split_class_name:

            # Compare the first 3 chars of the primitive family to make sure we are replacing the correct one
            # and not the wrong one like logistic_regression
            if not primitive_family.lower()[:2] == family[:2]:
                break
            split_class_name = split_class_name.replace("_{}".format(family), "")
            break

    # To make class names verbose
    expansions = {"_nb": "_naive_bayes"}
    for key, value in expansions.items():
        if key in split_class_name:
            split_class_name = split_class_name.replace(key, value)
            break

    # primitive['installation'] = [{'type': 'PIP', 'package_uri': 'git+https://gitlab.datadrivendiscovery.org/jpl/d3m_sklearn_wrap.git@{}'.format(tag)}]
    name_split = name.split(".")
    primitive['source'] = {'name': 'JPL', 'contact': 'mailto:shah@jpl.nasa.gov', 'uris': ['https://gitlab.com/datadrivendiscovery/sklearn-wrap/issues', 'https://scikit-learn.org/stable/modules/generated/' + str(".".join(name_split[:2]) + "." + name_split[-1]) + '.html']}
    primitive['python_path'] = "{}.{}.{}.{}".format("d3m.primitives",(template_vars.get('primitive_family', overlay.get('primitive_family'))).lower(), split_class_name.lower(), 'SKlearn')
    primitive['primitive_family'] = overlay.get(class_name).get('primitive_family', primitive_family)
    primitive['algorithm_types'] = overlay[class_name].get("algorithm_types")
    primitive['version'] = version
    if hyperparams_to_tune.get(class_name, {}).get("hyperparams_to_tune"):
        primitive['hyperparams_to_tune'] = hyperparams_to_tune.get(class_name).get("hyperparams_to_tune", [])
    replace_strings_to_enum(primitive)

    for key in metadata_keys:
        if key in primitive:
            if isinstance(primitive[key], str):
                if key == 'primitive_family':
                    metadata[key] = 'metadata_base.PrimitiveFamily.{}'.format(str(primitive[key]).upper())
                else:
                    metadata[key] = '"{}"'.format(primitive[key])
            else:
                metadata[key] = primitive[key]

    # Get semantic types to add or remove
    ADD_SEMANTIC_TYPES_KEY = "add_semantic_types"
    REMOVE_SEMANTIC_TYPES_KEY = "remove_semantic_types"
    add_semantic_types = overlay.get(class_name).get(ADD_SEMANTIC_TYPES_KEY, overlay.get(ADD_SEMANTIC_TYPES_KEY, []))
    remove_semantic_types = overlay.get(class_name).get(REMOVE_SEMANTIC_TYPES_KEY, overlay.get(REMOVE_SEMANTIC_TYPES_KEY, []))


    metadata['id'] = primitive_ids.get(metadata.get("python_path"))
    template_vars['metadata'] = metadata

    template_vars.update(process_predict_proba(primitive))
    template_vars.update(process_partial_fit(primitive))
    template_vars.update(contains_get_support(primitive))
    template_vars.update(contains_feature_importances(primitive))
    template_vars.update(contains_cluster_centers(primitive))
    template_vars.update({"iterate_columns": overlay.get(class_name).get("iterate_columns", False)})
    template_vars.update({"accepted_structural_types": overlay.get(class_name).get("accepted_structural_types", False)})
    template_vars.update({"accepted_semantic_types": overlay.get(class_name).get("accepted_semantic_types", False)})
    template_vars.update({"not_accepted_semantic_types": overlay.get(class_name).get("not_accepted_semantic_types", False)})
    template_vars.update({ADD_SEMANTIC_TYPES_KEY: add_semantic_types})
    template_vars.update({REMOVE_SEMANTIC_TYPES_KEY: remove_semantic_types})
    template_vars.update({"check_attribute_semantic_type": overlay.get(class_name).get("check_attribute_semantic_type", True)})
    template_vars.update(
        {"contain_any_accepted_semantic_types": overlay.get(class_name).get("contain_any_accepted_semantic_types", False)})

    return template_vars, metadata['id']


def generate_code(template_name, output_dir: str, primitive_file: str = None, primitive_json: dict = None,
                  overlay: dict = None, primitive_family: str = "CLASSIFICATION", version="0.1.0",
                  primitive_ids: dict=None, hyperparams_to_tune=None):
    """
    To auto generate code as specified in the template
    :param template_name: Name of template to use, see templates folder for available templates
    :param output_dir: Out directory to write into
    :param primitive_file: Primitive file to read json from
    :param primitive_json: Primitive json
    :param overlay: Overlay dictionary to add/override any values in the primitive json
    :return:
    """
    print("Processing {} ".format(primitive_file))
    if overlay is None:
        print("Need to provide an overlay file with Hyperparameter info")
        exit(1)

    if not os.path.exists(output_dir):
        os.mkdir(output_dir)
    wrapper = AutogenUtil.TemplateUtil()
    wrapper.load_env()


    template_vars = {}
    tag = "v0.1.0"
    if primitive_json is not None:
        template_vars, id = get_template_vars_from_primitive_json(primitive_json, overlay, tag, primitive_family, version,
                                                                  primitive_ids, hyperparams_to_tune)
    elif primitive_file is not None:
        template_vars, id = get_template_vars_from_primitive_file(primitive_file, overlay, tag, primitive_family, version,
                                                                  primitive_ids, hyperparams_to_tune)
    else:
        return None, None

    if template_vars is None:
        return None, None
    wrapper.load_template(template_vars.get('template', template_name))
    code_file = os.path.join(output_dir, "SK{}.py".format(template_vars.get('class_name')))
    code_str = wrapper.render(params=template_vars.get('params', []),
                              modules=template_vars.get('from_modules'),
                              hyperparams=template_vars.get('hyperparams'),
                              metadata=template_vars.get('metadata'),
                              class_name=template_vars.get('class_name'),
                              sklearn_class_name=template_vars.get("sklearn_class_name"),
                              constructor_args=template_vars.get('constructor_args'),
                              custom_imports=template_vars.get('custom_imports', []),
                              contains_log_proba=template_vars.get("contains_log_proba", False),
                              contains_proba=template_vars.get("contains_proba", False),
                              contains_get_support=template_vars.get("contains_get_support", False),
                              contains_feature_importances=template_vars.get("contains_feature_importances", False),
                              contains_cluster_centers=template_vars.get("contains_cluster_centers", False),
                              flags=template_vars.get("flags"),
                              iterate_columns=template_vars.get("iterate_columns", False),
                              accepted_structural_types=template_vars.get("accepted_structural_types"),
                              accepted_semantic_types=template_vars.get("accepted_semantic_types"),
                              not_accepted_semantic_types=template_vars.get("not_accepted_semantic_types", False),
                              check_attribute_semantic_type=template_vars.get("check_attribute_semantic_type", True),
                              add_semantic_types=template_vars.get("add_semantic_types"),
                              remove_semantic_types=template_vars.get("remove_semantic_types"),
                              contain_any_accepted_semantic_types=template_vars.get("contain_any_accepted_semantic_types"),
                              partial_fit=template_vars.get("partial_fit"))
    with open(code_file, 'w') as fw:
        fw.write(code_str)

    # Write out the test files
    print("Generating tests")
    output_dir = os.path.join(os.path.dirname(__file__), "..", "tests")

    test_wrapper = AutogenUtil.TemplateUtil()
    test_wrapper.load_env()
    test_wrapper.load_template(template_vars.get('test_template', overlay.get('test_template')))
    setup_str = test_wrapper.render(class_name="SK{}".format(template_vars.get('class_name')),
                                    test_data=template_vars.get("test_data"),
                                    contains_feature_importances=template_vars.get("contains_feature_importances", False),
                                    contains_log_proba=template_vars.get("contains_log_proba", False),
                                    contains_cluster_centers=template_vars.get("contains_cluster_centers", False),
                                    remove_semantic_types = template_vars.get("remove_semantic_types", False),
                                    add_semantic_types=template_vars.get("add_semantic_types", False))
    with open(os.path.join(output_dir, "test_SK{}.py".format(template_vars.get('class_name'))), 'w') as fw:
        fw.write(setup_str)

    output_dir = os.path.join(os.path.dirname(__file__), "..", "tests", "pipelines")

    pipeline_wrapper = AutogenUtil.TemplateUtil()
    pipeline_wrapper.load_env()
    pipeline_wrapper.load_template(template_vars.get('pipeline_template', overlay.get('pipeline_template')))
    setup_str = pipeline_wrapper.render(class_namespace=template_vars.get('metadata').get('python_path'),
                                        class_name="SK{}".format(template_vars.get('class_name')))
    with open(os.path.join(output_dir, "pipeline_SK{}.py".format(template_vars.get('class_name'))), 'w') as fw:
        fw.write(setup_str)

    return "SK{}".format(template_vars.get('class_name')), id


def main():
    base_path = os.path.join(os.path.dirname(__file__), "resources", "primitive-jsons")
    output_dir = "output"
    for file in os.listdir(output_dir):
        os.remove(os.path.join(output_dir, file))
    for file in os.listdir(os.path.join(os.path.dirname(__file__), "resources", "primitive-jsons")):
        generate_code(template_name="supervised.template",
                      primitive_file=os.path.join(base_path, file),
                      output_dir="output",
                      overlay=json.load(open(os.path.join(os.path.dirname(__file__), "resources", "overlay_classifiers.json"))),
                      primitive_family="CLASSIFICATION")
    # for file in os.listdir(os.path.join(os.path.dirname(__file__), "..", "tests", "resources", "primitive-jsons")):
    #     generate_code(template_name="supervised.template",
    #                   primitive_file=os.path.join(base_path, file),
    #                   output_dir="output",
    #                   overlay=json.load(open(os.path.join(os.path.dirname(__file__), "..", "tests", "resources", "overlay_regressors.json"))),
    #                   primitive_family="REGRESSION")
    # for file in os.listdir(os.path.join(os.path.dirname(__file__), "..", "tests", "resources", "primitive-jsons")):
    #     generate_code(template_name="unsupervised.template",
    #                   primitive_file=os.path.join(base_path, file),
    #                   output_dir="output",
    #                   overlay=json.load(open(os.path.join(os.path.dirname(__file__), "..", "tests", "resources", "overlay_preprocessors.json"))),
    #                   primitive_family="DATA_PREPROCESSING")
    #
    # # Used for testing individual files
    # test_file = "/Users/shah/Desktop/Development/d3m/sklearn-wrapping/tests/resources/primitive-jsons/ARDRegressions.json"
    # generate_code(template_name="supervised.template",
    #                   primitive_file=os.path.join(base_path, test_file),
    #                   output_dir="output",
    #                   overlay=json.load(open(os.path.join(os.path.dirname(__file__), "..", "tests", "resources", "overlay_regressors.json"))),
    #                   primitive_family="REGRESSION")
    #
    #
    # # Used for testing individual files
    # test_file = "/Users/shah/Desktop/Development/d3m/sklearn-wrapping/tests/resources/primitive-jsons/ARDRegressions.json"
    # generate_code(template_name="supervised.template",
    #                   primitive_file=os.path.join(base_path, test_file),
    #                   output_dir="output",
    #                   overlay=json.load(open(os.path.join(os.path.dirname(__file__), "..", "tests", "resources", "overlay_regressors.json"))),
    #                   primitive_family="REGRESSION")


if __name__ == '__main__':
    main()

