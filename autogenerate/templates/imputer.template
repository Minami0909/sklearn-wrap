{% extends "preprocessor.unsupervised.template" %}
{% block params %}
class Params(params.Params):{% for key, value in params.items() %}
    {{ key }}: Optional[{{ value}}]{% endfor %}
    input_column_names: Optional[pandas.core.indexes.base.Index]
    imputed_column_names: Optional[Sequence[str]]
    dropped_cols: Optional[Sequence[int]]
    target_names_: Optional[Sequence[Any]]
    training_indices_: Optional[Sequence[int]]
    target_column_indices_: Optional[Sequence[int]]
    target_columns_metadata_: Optional[List[OrderedDict]]

{% endblock %}
{% block get_set_params %}
    def get_params(self) -> Params:
        if not self._fitted:
            return Params({% for param in params.keys() %}
                {{ param }}=None,{% endfor %}
                input_column_names=self._input_column_names,
                imputed_column_names=self._imputed_column_names,
                dropped_cols=self._dropped_cols,
                training_indices_=self._training_indices,
                target_names_=self._target_names,
                target_column_indices_=self._target_column_indices,
                target_columns_metadata_=self._target_columns_metadata
            )

        return Params({% for param in params.keys() %}
            {{ param }}=getattr(self._clf, '{{ param }}', None),{% endfor %}
            input_column_names=self._input_column_names,
            imputed_column_names=self._imputed_column_names,
            dropped_cols=self._dropped_cols,
            training_indices_=self._training_indices,
            target_names_=self._target_names,
            target_column_indices_=self._target_column_indices,
            target_columns_metadata_=self._target_columns_metadata
        )

    def set_params(self, *, params: Params) -> None:{% for param in params.keys() %}
        self._clf.{{ param }} = params['{{ param }}']{% endfor %}
        self._input_column_names = params['input_column_names']
        self._imputed_column_names = params['imputed_column_names']
        self._dropped_cols = params['dropped_cols']
        self._training_indices = params['training_indices_']
        self._target_names = params['target_names_']
        self._target_column_indices = params['target_column_indices_']
        self._target_columns_metadata = params['target_columns_metadata_']
        {% for param in params.keys() %}
        if params['{{ param }}'] is not None:
            self._fitted = True{% endfor %}{% if params|length ==0 %}self._fitted = True{% endif %}
{% endblock %}

    {% block custom_class_variables -%}
        self._imputed_column_names = None
        self._dropped_cols = None
    {%- endblock %}
    {% block fit %}
    def fit(self, *, timeout: float = None, iterations: int = None) -> CallResult[None]:
        if self._fitted:
            return CallResult(None)

        self._training_inputs, self._training_indices, self._dropped_cols, columns_to_impute = self._get_columns_to_fit(self._inputs, self.hyperparams)
        self._input_column_names = self._training_inputs.columns.astype(str)

        if self._training_inputs is None:
            return CallResult(None)

        if len(self._training_indices) > 0:
            self._clf.fit(self._training_inputs)
            self._fitted = True
            if self.hyperparams['add_indicator']:
                self._imputed_column_names = ['{}_missing'.format(self._inputs.columns[idx]) for idx in range(len(self._inputs.columns)) if idx in columns_to_impute]
        else:
            if self.hyperparams['error_on_no_input']:
                raise RuntimeError("No input columns were selected")
            self.logger.warn("No input columns were selected")
        return CallResult(None)
        {% endblock %}
    {% block produce %}
    def produce(self, *, inputs: Inputs, timeout: float = None, iterations: int = None) -> CallResult[Outputs]:
        sk_inputs, columns_to_use, dropped_cols, _ = self._get_columns_to_fit(inputs, self.hyperparams)
        output = []
        if len(sk_inputs.columns):
            try:
                sk_output = self._clf.transform(sk_inputs)
            except sklearn.exceptions.NotFittedError as error:
                raise PrimitiveNotFittedError("Primitive not fitted.") from error
            if sparse.issparse(sk_output):
                sk_output = pandas.DataFrame.sparse.from_spmatrix(sk_output)
            target_columns_metadata = self._copy_columns_metadata(inputs.metadata, self._training_indices, self.hyperparams)
            output = self._wrap_predictions(inputs, sk_output, target_columns_metadata)

            col_names = [inputs.columns[idx] for idx in range(len(inputs.columns)) if idx not in self._dropped_cols and idx in columns_to_use]
            if self.hyperparams['add_indicator']:
                for i, column_index in enumerate(output.columns[len(col_names):]):
                    output.metadata = output.metadata.update_column(column_index, OrderedDict([('name', self._imputed_column_names[i]),
                                                                                               ('structural_type', 'int'),
                                                                                               ('semantic_types', ['https://metadata.datadrivendiscovery.org/types/Attribute'])]))
                col_names.extend(self._imputed_column_names)
            output.columns = col_names

            output = [output]
        else:
            if self.hyperparams['error_on_no_input']:
                raise RuntimeError("No input columns were selected")
            self.logger.warn("No input columns were selected")
        outputs = base_utils.combine_columns(return_result=self.hyperparams['return_result'],
                                               add_index_columns=self.hyperparams['add_index_columns'],
                                               inputs=inputs, column_indices=self._training_indices,
                                               columns_list=output)
        return CallResult(outputs)
        {% endblock %}

        {% block wrap_predictions %}def _wrap_predictions(self, inputs: Inputs, predictions: ndarray, target_columns_metadata) -> Outputs:
        outputs = d3m_dataframe(predictions, generate_metadata=False)
        outputs.metadata = self._update_predictions_metadata(inputs.metadata, outputs, target_columns_metadata)
        return outputs{% endblock %}

    {% block add_target_columns_metadata %}
    @classmethod
    def _copy_columns_metadata(cls, inputs_metadata: metadata_base.DataMetadata, column_indices, hyperparams) -> List[OrderedDict]:
        outputs_length = inputs_metadata.query((metadata_base.ALL_ELEMENTS,))['dimension']['length']

        target_columns_metadata: List[OrderedDict] = []
        for column_index in column_indices:
            column_name = inputs_metadata.query((metadata_base.ALL_ELEMENTS, column_index)).get("name")
            column_metadata = OrderedDict(inputs_metadata.query_column(column_index))
            semantic_types = set(column_metadata.get('semantic_types', []))
            semantic_types_to_remove = set(
            {%- if remove_semantic_types -%}
                [{%- for sem_type in remove_semantic_types -%}"{{ sem_type }}",{%- endfor -%}]
            {%- else -%}[]{%- endif %})
            add_semantic_types = {% if add_semantic_types|length > 0 -%}
                set([{%- for sem_type in add_semantic_types -%}"{{ sem_type }}",{%- endfor -%}])
            {%- else -%}set(){%- endif %}
            add_semantic_types.add(hyperparams["return_semantic_type"])
            semantic_types = semantic_types - semantic_types_to_remove
            semantic_types = semantic_types.union(add_semantic_types)
            column_metadata['semantic_types'] = list(semantic_types)

            column_metadata["name"] = str(column_name)
            target_columns_metadata.append(column_metadata)

        return target_columns_metadata{% endblock %}

    {% block get_columns_to_fit %}
    {% block _get_columns_to_fit %}
    @classmethod
    def _get_columns_to_fit(cls, inputs: Inputs, hyperparams: Hyperparams):

        if not hyperparams['use_semantic_types']:
            columns_to_produce = list(range(len(inputs.columns)))

        else:
            inputs_metadata = inputs.metadata

            def can_produce_column(column_index: int) -> bool:
                return cls._can_produce_column(inputs_metadata, column_index, hyperparams)

            columns_to_produce, columns_not_to_produce = base_utils.get_columns_to_use(inputs_metadata,
                                                                                 use_columns=hyperparams['use_columns'],
                                                                                 exclude_columns=hyperparams['exclude_columns'],
                                                                                 can_use_column=can_produce_column)

        columns_to_drop, columns_to_impute = cls._get_columns_to_drop(inputs, columns_to_produce, hyperparams)
        if hyperparams['add_indicator']:
            for col in columns_to_drop:
                columns_to_produce.remove(col)

        return inputs.iloc[:, columns_to_produce], columns_to_produce, columns_to_drop, columns_to_impute{% endblock %}

    {% block columns_to_drop %}@classmethod
    def _get_columns_to_drop(cls, inputs: Inputs, column_indices: List[int], hyperparams: Hyperparams) -> Tuple[List[int], List[int]]:
        """
        Check for columns that contain missing_values that need to be imputed
        If strategy is constant and missin_values is nan, then all nan columns will not be dropped
        :param inputs:
        :param column_indices:
        :return:
        """
        columns_to_remove = []
        columns_to_impute = []
        if hyperparams['strategy'] != "constant":
            for col in column_indices:
                    inp = inputs.iloc[:, [col]].values
                    mask = _get_mask(inp, hyperparams['missing_values'])
                    if mask.all():
                        columns_to_remove.append(col)
                    elif mask.any():
                        columns_to_impute.append(col)
        return columns_to_remove, columns_to_impute{% endblock %}

    @classmethod
    def _can_produce_column(cls, inputs_metadata: metadata_base.DataMetadata, column_index: int, hyperparams: Hyperparams) -> bool:
        column_metadata = inputs_metadata.query((metadata_base.ALL_ELEMENTS, column_index))

        accepted_structural_types = {% block accepted_structural_types %}
        {%- if accepted_structural_types -%}
            ({%- for str_type in accepted_structural_types -%}{{ str_type }},{%- endfor -%})
        {%- else -%}(int, float, numpy.integer, numpy.float64){%- endif -%}{% endblock %}
        accepted_semantic_types = {% block accepted_semantic_types %}
        {%- if accepted_semantic_types -%}
            set([{%- for sem_type in accepted_semantic_types -%}"{{ sem_type }}",{%- endfor -%}])
        {%- else -%}set(){%- endif -%}{% endblock %}
        accepted_semantic_types.add("https://metadata.datadrivendiscovery.org/types/Attribute")
        if not issubclass(column_metadata['structural_type'], accepted_structural_types):
            return False

        semantic_types = set(column_metadata.get('semantic_types', []))

        if len(semantic_types) == 0:
            cls.logger.warning("No semantic types found in column metadata")
            return False
        # Making sure all accepted_semantic_types are available in semantic_types
        if len(accepted_semantic_types - semantic_types) == 0:
            return True

        return False{% endblock %}
