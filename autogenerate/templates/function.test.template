import unittest
import pickle

from autogenerate.d3m_sklearn_wrap.sklearn_wrap import {{ class_name }}
from tests.params_check import params_check
from d3m.metadata import base as metadata_base
from d3m import container
from d3m.primitive_interfaces.base import PrimitiveBase
from d3m.exceptions import PrimitiveNotFittedError
from pandas.util.testing import assert_frame_equal

import os
from common_primitives import dataset_to_dataframe, column_parser

{% block dataset %}
{%- if test_data == None -%}
{% set test_data = "tests-data/datasets/iris_dataset_1" %}{% endif %}
{% include "test_data.template" %}
{% endblock %}
{% block data_split %}{% endblock %}


# We want to test the running of the code without errors and not the correctness of it
# since that is assumed to be tested by sklearn

class Test{{ class_name }}(unittest.TestCase):
    def create_learner(self, hyperparams):
        clf = {{ class_name }}.{{ class_name }}(hyperparams=hyperparams)
        return clf

    def produce_learner(self, learner, **args):
        return learner.multi_produce(produce_methods=['produce'], **args)

    def basic_fit(self, hyperparams):
        learner = self.create_learner(hyperparams)
        training_data_args = self.set_data(hyperparams)

        output = self.produce_learner(learner, inputs=training_data_args.get("inputs"),
                                      second_inputs=training_data_args.get("outputs"))
        return output, learner, training_data_args

    def pickle(self, hyperparams):
        output, learner, training_data_args = self.basic_fit(hyperparams)

        # Testing get_params() and set_params()
        params = learner.get_params()
        learner.set_params(params=params)

        model = pickle.dumps(learner)
        new_clf = pickle.loads(model)
        new_output = new_clf.multi_produce(produce_methods=['produce'], inputs=training_data_args.get("inputs"),
                                           second_inputs=training_data_args.get("outputs"))

        assert_frame_equal(new_output.values['produce'], output.values['produce'])

    def set_data(self, hyperparams):
        hyperparams = hyperparams.get("use_semantic_types")
        if hyperparams:
            return {"inputs": train_set, "outputs": targets}
        else:
            return {"inputs": parsed_dataframe.select_columns([1, 2, 3, 4]),
                    "outputs": parsed_dataframe.select_columns([5])}

    def get_transformed_indices(self, learner):
        return learner._target_column_indices

    def new_return_checker(self, output, indices):
        assert output.metadata.has_semantic_type((metadata_base.ALL_ELEMENTS, metadata_base.ALL_ELEMENTS),
                                                 'https://metadata.datadrivendiscovery.org/types/Attribute') == True

    def test_with_semantic_types(self):
        hyperparams = {{ class_name }}.Hyperparams.defaults().replace({"use_semantic_types": True})
        self.pickle(hyperparams)

    def test_without_semantic_types(self):
        hyperparams = {{ class_name }}.Hyperparams.defaults()
        self.pickle(hyperparams)

    def test_with_new_return_result(self):
        hyperparams = {{ class_name }}.Hyperparams.defaults().replace({"return_result": 'new', "use_semantic_types": True})
        output, clf, _ = self.basic_fit(hyperparams)
        indices = self.get_transformed_indices(clf)
        self.new_return_checker(output.values['produce'], indices)


if __name__ == '__main__':
    unittest.main()