-e git+https://gitlab.com/datadrivendiscovery/d3m.git@devel#egg=d3m
Jinja2==2.9.4
simplejson==3.12.0
scikit-learn==0.22.2.post1
-e git+https://gitlab.com/datadrivendiscovery/common-primitives.git@devel#egg=d3m-common-primitives
